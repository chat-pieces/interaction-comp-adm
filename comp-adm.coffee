run = require '@aurium/run'

module.exports = compAdm = (bot, update)->
    msg = (update.message?.text or '').trim()
    usr = update.message?.from.username or ''

    return unless bot.isAdm usr
    runCmd bot, update if msg.substr(0,3) is '/sh'
    download bot, update if msg.substr(0,9) is '/download'


formatStdout = (stdout)->
    "<pre>#{stdout
        .replace /^[\n\r]*/, ''
        .replace /&/, '&amp;'
        .replace /</, '&lt;'
        .replace />/, '&gt;'
    }</pre>"

baseEnv =
    PATH: '/usr/local/bin:/usr/bin:/bin'
    USER: process.env.USER or (process.getuid() is 0 and 'root' or '')
    HOME: process.env.HOME

runCmd = (bot, update)->
    chat = update.message.chat.id or 0
    msgId = update.message.message_id
    cmd = update.message.text.trim().substr(3).trim()
    runner = run 'sh', '-c', cmd, cwd: '/tmp', env: baseEnv
    stdout = ''
    stdoutWasSend = false
    intervalId = null
    apiParams = reply_to_message_id: msgId, parse_mode: 'HTML'

    runner.proc.stdout.on 'data', (data)->
        data.toString().split(/\r*\n\r*|\r/).forEach (line)->
            stdout += '\n' + line
            if stdout.length > 800
                bot.sendMessage formatStdout(stdout), chat, apiParams
                stdout = ''
                stdoutWasSend = true

    intervalId = setInterval (->
        if stdout.trim().length > 0
            bot.sendMessage formatStdout(stdout), chat, apiParams
            stdout = ''
            stdoutWasSend = true
        else
            bot.sendMessage "<i>Esperando...</i>", chat, apiParams
    ), 5000

    runner
    .then ->
        if stdout.trim().length > 0
            bot.sendMessage formatStdout(stdout), chat, apiParams
        else if not stdoutWasSend
            bot.sendMessage "<b>Finish without output.</b>", chat, apiParams
    .catch (err)->
        bot.sendMessage "<b>FAIL</b> #{formatStdout err.message}", chat, apiParams
    .then ->
        clearTimeout intervalId


download = (bot, update)->
    chat = update.message?.chat.id or 0
    msgId = update.message.message_id
    path = update.message.text.trim().substr(9).trim()
    bot.sendMessage "TODO: sendFile #{path}", chat, reply_to_message_id: msgId

